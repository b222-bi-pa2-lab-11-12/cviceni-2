# Cvičení 2

## Úvod

V minulém cvičení jsme si předvedli, jak kod z jazyka C přepsat do C++, ovšem to byla poze špička ledovce. 
V dnešním cvičení si ukážeme v rámci úvodu do Objektově orientovaného programování (dále jen OOP), jak se
dá použít v rámci zapozdřených struktur.

## Git
Naklonujte si prosím tento repozitář, v rámci celého cvičení s ním budeme pracovat
```
git clone https://gitlab.fit.cvut.cz/b222-bi-pa2-lab-11-12/cviceni-2.git
```

## Makefile
V tomto cvičení již dáváme k dispozici make file. Pomocí něj je možné soubory kompilovat a zároveň jsme do něj přidali
automatizované testy, které ověří funkčnost vašeho řešení.

## Něco na rozehřátí
Ve složce warmup naleznete 3 soubory:
- [NotNumber.h](warmup%2FNotNumber.h) - předpřipravený hlavičkový soubor třídy NotNumber
- [NotNumber.cpp](warmup%2FNotNumber.cpp) - soubor na implementace method třidy NotNumber
- [test.cpp](warmup%2Ftest.cpp) - soubor s testem (NEUPRAVUJTE)

Třída NotNumber uchovává číslo (`int`), umožňuje ho získat i změnit. U té změny je však háček. Třída nikdy neumožní
uložit čislo, co se nachází v konstantě `NOT_NUMBER`.

Přidejte implementace method `NotNumber` (výchozí konstruktor), `set` a `get` tak aby:
- po vytvření instace třídy (objektu) v ní bylo uloženo číslo z constanty `NOT_NUMBER_DEFAULT`
- metoda `get` vrátila vždy aktuálně uložené číslo
- metoda `set` uloží číslo z argumentu do objektu, ale jen za předpokladu, že není rovné `NOT_NUMBER`. Pokud je, metoda neudela nic.
  Metoda vrací samotný objekt NotNumber (fluent access).

<details>
<summary>Nápověda</summary>

> V souboru [NotNumber.h](warmup%2FNotNumber.h) je nutné si přidat atribut třídy, do kterého si objekt bude číslo ukládat.
> 
> V souboru [NotNumber.cpp](warmup%2FNotNumber.cpp) je nutné vytvořit implementace. Jejich základní tvar získáte v CLionu
> pomocí práveho tlačítka na jednotlivé metody v souboru [NotNumber.h](warmup%2FNotNumber.h) -> Show context action -> 
> Generate definition...
> 
> Metoda `NotNumber`, díky tomu že sdílí název ze třidou, je automaticky construktorem. Tím že nemá argumenty, jedná se 
> o konstrkutor, který je volaný v případě vytvoření nové instance třídy. Construktory nikdy nic nevrací.
> 
> Pro navrácení objektu stačí zapsat za return `*this`


</details>

Po dokončení je možné provést test pomocí příkazu `make warmup-test`.   
(Pokud používáte CMake, jedná se executable `warmup`)


## Úlohy na toto cvičení

Ve složce main se nachází soubor [pt4.cpp](main%2Fpt4.cpp). To je náš starý známí z minulého cvičení, ale již obsahuje 
pouze funkce pracující se strkturou `IntVec`. Naším cílem bude tuto strukturu předělat na třídu. Na to jsou v téže složce
připravený soubory [TypeVec.cpp](main%2FTypeVec.cpp) a [TypeVec.h](main%2FTypeVec.h). Třída nebude mít žádné veřejné atributy
(členské proměnné).

Každou z úloh je po jejím dokončení možné otestovat pomocí `make test{CISLO_ULOHY}` ve složce main  
(Případě CMake `test1`, `test2`, `test3` a `test4`)

### 1) Implementace základních metod
Vytvořte definice method v souboru [TypeVec.cpp](main%2FTypeVec.cpp) tak, aby odpovídali hlavičce v souboru [TypeVec.h](main%2FTypeVec.h).
Zkopírujte do nich obsah funkcí ze souboru [pt4.cpp](main%2Fpt4.cpp) a nově vzniklé metody upravte tak, aby to byli metody. Metody taky upravte tak
aby místo typu `int` pracovali s typem `VecType`

<details>
<summary>Nápověda</summary>

> Jedná se o stejný postup, jako v zahřívací úloze, jen je toho více

</details>

### 2) Z metod, u kterých je to možné, udělejte metody konstantní
Některé z implementovaných metod nijak neupravují atributy instance třídy. Tyto metody označnte jako konstantní.

<details>
<summary>Nápověda</summary>

> Pokud metoda neukládá do atributů objektů, stačí za `)` ta parametry zapsat `const`

</details>

### 3) Implementujte funkci fillTheTypeVec
Implementujte funkci tak, aby vytvořila VecType naplněný čísli od 1 do parametru `count` (vzestupně).

<details>
<summary>Nápověda</summary>

> Vytvořte si proměnnou typu `TypeVec` zápisem:
> ```c++
> TypeVec jmenoPromenne(size);
> ```
> díky tomu, že nemáme defaultní construktor je nutné hodnotu pro explictiní konstruktor dodat už v rámci tvoření proměnné
> 
> Následně za použítí nějakého cyklu a metod zápisem `jmenoPromenne.jmenoMetody(argumenty)` strukturu modifikujte a následně vraťte.

</details>

### 4) Přidejte do třídy počítadlo volaní metody `at`
Kdykoliv bude zavolána metoda at (nad jakoukoliv instancí této třídy), toto počítadlo se zvedne o 1. Počítadlo začíná na 0.

Přidejte zároveň metodu, která vrátí hodnotu tohoto počítadla. Tuto metodu pojmenujte `getCountAt`, nebude přijímat žádný
argument a bude vracet `size_t`.

<details>
<summary>Nápověda</summary>

> je potřeba klíčového slovva `static` (jak pro promennou pocitadla, tak pro metodu, co se na jeho hodnotu kouká).

</details>
