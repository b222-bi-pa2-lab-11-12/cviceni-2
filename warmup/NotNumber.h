#ifndef NOTNUMBER_H
#define NOTNUMBER_H

/**
 * Number, which can't be stored in NotNumber
 */
const int NOT_NUMBER = 5;

/**
 * Default number for NotNumber after creation
 */
const int NOT_NUMBER_DEFAULT = 42;

class NotNumber {
    //TODO fill needed internals
public:
    NotNumber();
    NotNumber & set(int num);
    int get();
};


#endif //NOTNUMBER_H