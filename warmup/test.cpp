#include "NotNumber.h"
#include <cassert>
#include <iostream>

int main() {
    NotNumber x;
    assert(x.get() == NOT_NUMBER_DEFAULT);
    assert(&x.set(10) == &x);
    assert(x.get() == 10);
    x.set(6).set(NOT_NUMBER);
    assert(x.get() == 6);
    std::cout << "SUCCESS" << std::endl;
    return EXIT_SUCCESS;
}