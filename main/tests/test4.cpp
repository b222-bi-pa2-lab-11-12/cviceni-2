#include "../TypeVec.h"
#include <cassert>
#include <iostream>
#include <sstream>

int main() {
    TypeVec a(10);
    TypeVec b(10);
    TypeVec c(10);
    assert(TypeVec::getCountAt() == 0);
    a.append(0);
    assert(TypeVec::getCountAt() == 0);
    a.at(0);
    assert(TypeVec::getCountAt() == 1);
    b.append(0);
    c.append(0);
    b.at(0);
    b.at(0);
    b.at(0);
    b.at(0);
    b.at(0);
    b.at(0);
    c.at(0);
    a.at(0);
    assert(TypeVec::getCountAt() == 9);
    cout << "SUCCESS" << endl;
}
