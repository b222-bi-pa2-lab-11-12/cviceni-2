#include "../TypeVec.h"
#include <cassert>
#include <iostream>
#include <sstream>

int main() {
    stringstream sout;
    TypeVec a(2);
    assert (&a.append(10) == &a);
    a
    .append(20)
    .append(30)
    .append(50);

    sout.str( "" );
    a.print(sout);
    assert( sout.str() == "10 20 30 50 \n");

    TypeVec b = a.generatePartialSums();
    sout.str( "" );
    b.print(sout);
    assert( sout.str() == "10 30 60 110 \n");

    TypeVec c = a.generateIntervalSums();
    sout.str( "" );
    c.print(sout);
    assert( sout.str() == "20 30 50 20 40 30 \n");
    assert(c.last() == 30);
    assert(c.at(2) == 50);
    cout << "SUCCESS" << endl;
}
