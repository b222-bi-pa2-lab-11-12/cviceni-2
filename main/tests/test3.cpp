#include "../TypeVec.h"
#include <cassert>
#include <iostream>
#include <sstream>

int main() {
    stringstream sout;
    sout.str("");
    TypeVec a = fillTheTypeVec(5);
    a.print(sout);
    assert(sout.str() == "1 2 3 4 5 \n");
    sout.str("");
    TypeVec b = fillTheTypeVec(42);
    b.print(sout);
    assert(sout.str() == "1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 \n");
    cout << "SUCCESS" << endl;
}
