#include "../TypeVec.h"
#include <cassert>
#include <iostream>
#include <sstream>

int main() {
    stringstream sout;
    TypeVec a(10);
    a
            .append(20)
            .append(30)
            .append(50);

    const TypeVec & b = a;
    b.last();
    b.print(sout);
    b.at(0);
    b.generatePartialSums();
    b.generateIntervalSums();
    cout << "SUCCESS" << endl;
}
