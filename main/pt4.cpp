#include <cstdlib>
#include <iostream>

using namespace std;

struct IntVec {
    int *m_buf;
    size_t m_len;
    size_t m_cap;
};

IntVec iv_new(size_t cap) {
    int *buf = new int[cap];
    IntVec vec = {buf, 0, cap};
    return vec;
}

int iv_at(IntVec &v, size_t i) { return v.m_buf[i]; }

int iv_last(IntVec &v) { return iv_at(v, v.m_len - 1); }

int iv_append(IntVec &v, int i) {
    if (v.m_len == v.m_cap) {
        int new_cap = v.m_cap * 2;
        int *new_buf = new int[new_cap];
        copy(v.m_buf, v.m_buf + v.m_len, new_buf);
        delete[] v.m_buf;
        v.m_buf = new_buf;
        v.m_cap = new_cap;
    }
    v.m_buf[v.m_len] = i;
    v.m_len++;

    return 0;
}

int iv_free(IntVec &v) {
    delete[] v.m_buf;
    return 0;
}

int iv_print(ostream &dst, IntVec &v) {
    for (size_t i = 0; i < v.m_len; i++) {
        dst << iv_at(v, i) << " ";
    }
    dst << endl;
    return 0;
}

IntVec iv_generate_partial_sums(IntVec &int_vector) {
    IntVec partial_sums = iv_new(int_vector.m_cap);
    iv_append(partial_sums, iv_at(int_vector, 0));
    for (size_t i = 1; i < int_vector.m_len; i++) {
        iv_append(partial_sums,
                  iv_at(int_vector, i) + iv_at(partial_sums, i - 1));
    }
    return partial_sums;
}

IntVec iv_generate_interval_sums(IntVec &partial_sums) {
    IntVec interval_sums = iv_new(partial_sums.m_cap);
    for (size_t i = 0; i < partial_sums.m_len - 1; i++) {
        for (size_t j = i + 1; j < partial_sums.m_len; j++) {
            int previous_partial = i == 0 ? 0 : iv_at(partial_sums, i - 1);
            iv_append(interval_sums, iv_at(partial_sums, j) - previous_partial);
        }
    }
    return interval_sums;
}
