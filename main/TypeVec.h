#ifndef INTVEC_H
#define INTVEC_H

#include <cstdlib>
#include <iostream>

using namespace std;

typedef int VecType;
const size_t DEFAULT_SIZE = 10;

class TypeVec {
private:
    int * m_buf;
    size_t m_len;
    size_t m_cap;
public:
    explicit TypeVec(size_t cap);
    ~TypeVec();
    VecType at(size_t i);
    VecType last();
    TypeVec & append(VecType i);
    ostream & print(ostream &dst);
    TypeVec generatePartialSums();
    TypeVec generateIntervalSums();
};

//TODO part3
TypeVec fillTheTypeVec(size_t count);

#endif //INTVEC_H
